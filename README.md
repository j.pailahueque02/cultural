<div align="center">
    <a href="https://flutter.io/">
        <img src="https://diegolaballos.com/files/images/flutter-icon.jpg" alt="Logo" width=72 height=72>
        <h2>Flutter Cultural App</h2>
    </a>
</div>

## Documentation

* [Install Flutter](https://flutter.dev/get-started/)
* [Flutter documentation](https://flutter.dev/docs)

