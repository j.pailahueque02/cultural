
import 'package:cultural/src/pages/event/event_category.dart';
import 'package:cultural/src/pages/event/event_details_page.dart';
import 'package:cultural/src/pages/home_page.dart';
import 'package:cultural/src/pages/login_page.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      initialRoute: HomePage.routeName,
      routes: {
        LoginPage.routeName         : (BuildContext context) => LoginPage(),
        HomePage.routeName          : (BuildContext context) => HomePage(),
        EventDetailPage.routeName   : (BuildContext context) => EventDetailPage(),
        EventCategoryPage.routeName : (BuildContext context) => EventCategoryPage(),
      },
    );
  }
}

