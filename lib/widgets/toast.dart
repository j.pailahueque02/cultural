import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void showToast(BuildContext context, String msg) {
  
  Scaffold.of(context).showSnackBar(
    SnackBar(
      content: Text(msg, textAlign: TextAlign.center),
      duration: Duration(seconds: 1),
    )
  );
}