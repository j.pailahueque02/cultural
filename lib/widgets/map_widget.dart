import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';

TileLayerOptions map() {
  return TileLayerOptions(
      backgroundColor: Colors.black87,
      urlTemplate: 'https://api.mapbox.com/v4/'
      '{id}/{z}/{x}/{y}@2x.png?access_token={accessToken}',
      additionalOptions: {
        'accessToken': 'pk.eyJ1Ijoiam9yZ2UwMDc4IiwiYSI6ImNrM3Y1c3N4djBraTYza28xNm5peXRxcm4ifQ.6A-luhW7KtPJaTrViTGYsg',
        'id': 'mapbox.streets',
      }
  );
} 

