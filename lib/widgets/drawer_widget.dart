import 'package:cultural/src/pages/home_page.dart';
import 'package:flutter/material.dart';
class DrawerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final String currentRoute = ModalRoute.of(context).settings.name; 
    return Drawer(
      child: Stack(
        children: <Widget>[
          _background(),
          ListView(
            children: <Widget>[
              _drawerHeader(),
              _itemDrawer( 
                name: 'Home',
                icon: Icons.cloud,
                currentRoute: currentRoute,
                routeName: HomePage.routeName,
                context: context,
              ),              
              // _itemDrawer( 
              //   name: 'Login design 1',
              //   icon: Icons.sentiment_very_satisfied,
              //   currentRoute: currentRoute,
              //   routeName: LoginDesign1.routeName,
              //   context: context,
              // ),
              // _itemDrawer( 
              //   name: 'Login design 2',
              //   icon: Icons.sentiment_satisfied,
              //   currentRoute: currentRoute,
              //   routeName: LoginPage.routeName,
              //   context: context,
              // ),
              // _itemDrawer( 
              //   name: 'Home 2',
              //   icon: Icons.airplanemode_active,
              //   currentRoute: currentRoute,
              //   routeName: HomePage2.routeName,
              //   context: context,
              // )
            ],
          )
        ],
      ),
    );
  }

  Widget _background() {
    return Container(
      height: double.maxFinite,
      width: double.maxFinite,
      color: Color.fromRGBO(54, 52, 63, 1.0),
    );
  }

  Widget _drawerHeader() {
    return DrawerHeader(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          CircleAvatar(
            backgroundColor: Colors.black,
            radius: 40.0,
            backgroundImage: NetworkImage('https://images.unsplash.com/photo-1568308389933-4d5b260272ff?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1534&q=80'),
          ),
          SizedBox(width: 10.0),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('Negro Matapacos', style: TextStyle(color: Colors.white70)),
              Row(
                children: <Widget>[
                  Icon(Icons.location_on, size: 13, color: Colors.white54),
                  Text('Temuco', style: TextStyle(color: Colors.white54))
                ],
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _itemDrawer({ String name, IconData icon, String routeName, String currentRoute, BuildContext context }) {
    return ListTile(
      contentPadding: EdgeInsets.symmetric(horizontal: 50.0),
      leading: Icon(icon, color: Colors.white70),
      title: Text(name, style: TextStyle(color: Colors.white70)),
      onTap: () {
        if (currentRoute == routeName) {
          Navigator.pop(context);
          return;
        }
        Navigator.pop(context);
        Navigator.pushReplacementNamed(context, routeName);
      },
    );
  }
  
}