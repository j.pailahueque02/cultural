import 'dart:convert';

Event eventModelFromJson(String str) => Event.fromJson(json.decode(str));
String eventModelToJson(Event data)  => json.encode(data.toJson());

class Event {
  String id;
  String titulo;
  String descripcion;
  String direccion;
  String fecha;
  String horario;
  int categoriaId;
  double latitud;
  double longitud;
  String urlImagen;
  String nombreCategoria;

  Event({
    this.id,
    this.titulo,
    this.descripcion,
    this.direccion,
    this.fecha,
    this.horario,
    this.categoriaId,
    this.latitud,
    this.longitud,
    this.urlImagen,
    this.nombreCategoria
  });

  factory Event.fromJson(Map<String, dynamic> json) => Event(

    id : json['id'],
    titulo : json['titulo'], 
    descripcion : json['descripcion'], 
    direccion : json['direccion'], 
    fecha : json['fecha'], 
    horario : json['horario'], 
    categoriaId : json['id_categoria'], 
    latitud : json['latitud'], 
    longitud : json['longitud'], 
    urlImagen : json['url_imagen'],
    nombreCategoria: json['nombre_categoria']
  );

  Map<String, dynamic> toJson() => {
    'id': id,
    'titulo': titulo,
    'descripcion': descripcion,
    'direccion': direccion,
    'fecha': fecha,
    'horario': horario,
    'categoriaId': categoriaId,
    'latitud': latitud,
    'longitud': longitud,
    'urlImagen': urlImagen
  };
}

