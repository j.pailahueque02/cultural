import 'dart:convert';
import 'dart:io';
import 'package:cultural/src/models/event_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class EventService {

  // final String _url = 'http://192.168.43.107:8000/api/v1';
  final String _url = 'https://tmovi-a6c1c.firebaseio.com';
  final String _urlApi = 'http://69.164.198.53:3617/public/sensor/147678';

  Future<bool> createProduct( Event productModel ) async {

    final url = '$_url/products.json';
    final requestHeaders = {
      'Content-type': 'application/json',
    };

    try {
      final resp = await http.post( url, headers: requestHeaders, body: eventModelToJson(productModel) );
      
      if ( resp.statusCode != 200 ) return false;
      return true;
    } catch (exception) {
      return false;
    }
  }

  Future<bool> updateProduct( Event productModel, String id ) async {

    final url = '$_url/products/$id.json';
    final requestHeaders = {
      'Content-type': 'application/json',
    };

    try {
      final resp = await http.put( url, headers: requestHeaders, body: eventModelToJson(productModel) );
      
      if ( resp.statusCode != 200 ) return false;
      return true;
    } catch (exception) {
      return false;
    }
  }

  Future<List<Event>> getProducts() async {

    List<Event> products = List();
    final url = '$_url/products.json';
    final resp = await http.get( url );
    final Map<String, dynamic> decodeJson = json.decode( resp.body );
    
    if ( decodeJson == null ) return [];

    decodeJson.forEach((key, values) {
      final productModel = Event.fromJson( values );
      // productModel.id = key;
      products.add(productModel);
    });

    return products;
  }

  Future<bool> deleteProduct( String id ) async {

    final url = '$_url/products/$id.json';

    try {
      final resp = await http.delete(url);
      if ( resp.statusCode != 200 ) return false;

      return true;
    } catch (exception) {
      return false;
    }
  }

  Future<String> uploadImage(BuildContext context, File image ) async {

    final uri   = Uri.parse('https://api.cloudinary.com/v1_1/dgto9ukor/image/upload?upload_preset=ui3rtqzx');
    // final types = mime( image.path ).split('/'); // ( image/jpg ) 

    final imageUploadRequest = http.MultipartRequest( 'POST', uri );

    final file = await http.MultipartFile.fromPath( 
      'file', 
      image.path,
      // contentType: MediaType( types[0], types[1] ),
    );

    imageUploadRequest.files.add(file);

    try {
      final streamResponse = await imageUploadRequest.send();
      final resp = await http.Response.fromStream( streamResponse );

      if ( resp.statusCode != 200 && resp.statusCode != 201 ) {
        // showToast(context,'Error al cargar imagen cloudinary');
        return null;
      }

      final respData = json.decode( resp.body );
      return respData['secure_url'];

    } catch (exception) {
      // showToast(context,'Error al cargar imagen');
      return null;
    }
  }

}
    // Map<String, String> requestHeaders = {
    //   'Authorization'    : 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6ODAwMC9hcGkvdjEvbG9naW4iLCJpYXQiOjE1NzczMDc0NjcsImV4cCI6MTU3NzMxMTA2NywibmJmIjoxNTc3MzA3NDY3LCJqdGkiOiJzdE52WjZoV1J6c05waGFzIn0.ZGg9ZYpjTWQsXrriAWDxK1NBNz73e1Kg1InSFAUj3f4',

    //   'Content-type': 'application/json',
    //   'X-Requested-With' : 'XMLHttpRequest'
    // };

  
    // final market = MarketModel( 
    //   marketCode: 'aodoi44', 
    //   marketName: 'adioa',
    //   marketDescription: "",
    //   marketLongDescription: '', 
    //   marketRoundMin: 2,
    //   marketRoundMax: 2, 
    //   currencyId: 2,
    //   levelId: 1
    // );