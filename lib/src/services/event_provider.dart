import 'dart:convert';
import 'package:cultural/src/models/event_model.dart';
import 'package:http/http.dart' as http;

class EventProvider {

  final String _urlApi = 'http://69.164.198.53:3617/public/sensor/147678';
  final String _urlFb = 'https://bd-app-movil-eventos.firebaseio.com';
  final String _token = '00147132-001e-4b4e-853b-681d07ef2295';
  final String _providerToken= 'false';

  Future<Map<String, dynamic>> getEventsApi() async {
    

    try {

      final resp = await http.get(_urlApi+'?token=$_token'+'&provider_token=$_providerToken');
      final Map<String, dynamic> decodeData = json.decode( resp.body );
      // final Map<String, dynamic> decode = json.decode(decodeData['data']);  

      return { 'ok': true, 'status': decodeData['data']};
    } catch (exception) {
      return { 'ok': false, 'message': 'Failed connection' };
    }
  }

  Future<List<Event>> getEventsFirebase() async {
      final resp = await http.get('https://bd-app-movil-eventos.firebaseio.com/eventos.json');
      final Map<String, dynamic> decodeData = json.decode( resp.body );

      final List<Event> events = new List();

      if (decodeData == null) return [];

      decodeData.forEach((id, event) {
        final currevent = Event.fromJson(event);
        currevent.id = id;
        events.add(currevent);
      });



      return events;
  }


}