import 'package:cultural/widgets/map_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';

class RecyclePage extends StatefulWidget {
  @override
  _RecyclePageState createState() => _RecyclePageState();
}

class _RecyclePageState extends State<RecyclePage> {
  @override
  Widget build(BuildContext context) {
    return FlutterMap(
      options: new MapOptions(
        center: new LatLng(-38.746202450627074, -72.61593475779421),
        zoom: 13.0,
      ),
      layers: [
        map(),
        _createMarkers(new LatLng(-38.746202450627074, -72.61593475779421)),
        _createMarkers(new LatLng(-38.748244139708156, -72.61351004084474)),
      ],
    );
  }

  _createMarkers(LatLng ltlg) {
    return MarkerLayerOptions(
      markers: <Marker>[
        Marker(
          height: 100.0,
          width: 150.0,
          point: ltlg,
          builder: (context) => Center(
            child: Container(
              child: Stack( 
                children: <Widget>[
                  Center(
                    child: Icon(Icons.location_on, color: Colors.red, size: 40.0),
                  ),
                  Positioned(
                    child: Card(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.symmetric(vertical: 3.0),
                            color: Colors.green,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Container(
                                  child: Text('Punto limpio', 
                                    textAlign: TextAlign.center, 
                                    overflow: TextOverflow.clip,
                                    style: TextStyle(
                                      color: Colors.white
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  ),
                ],
              ),
            ),
          )
        ),
      ]
    );
  }
}