import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CategoryPage extends StatefulWidget {
  static final routeName = "category";

  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  
  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;
    return Stack(
      children: <Widget>[
        Container( color: Color.fromRGBO(242, 241, 246, 1.0) ),
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              _categoryRow(_screenSize, 0, 1, FontAwesomeIcons.music, FontAwesomeIcons.commentAlt),
              _categoryRow(_screenSize, 2, 3, FontAwesomeIcons.leaf, FontAwesomeIcons.flag),
              _categoryRow(_screenSize, 4, 5, FontAwesomeIcons.volleyballBall, FontAwesomeIcons.laughWink),
            ],
          ),
        ),
      ],
    );
  }


  Widget _categoryRow(Size screen, int category1, int category2, IconData icon1, IconData icon2) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        _categoryCard(screen, category1, icon1),
        _categoryCard(screen, category2, icon2)
      ],
    );
  }

  Widget _categoryCard(Size screen, int category, IconData icon) {
    return Card(
      child: InkWell(
        splashColor: Theme.of(context).primaryColor,
        highlightColor: Theme.of(context).primaryColor,
        onTap: () {
          Navigator.pushNamed(context, 'event_category', arguments: _titleCard(category));  
        },
        child: Container(
          height: screen.height/4,
          width: screen.width/2 -10,
          child: _categoryItem(screen, icon, category),
        ),
      ),
    );
  }

  Widget _categoryItem(Size screen, IconData icon, int category) {
    return Container(
      height: screen.height/4,
      width: screen.width/2 -10,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(icon, size: 50.0, color: Theme.of(context).primaryColor),         
          SizedBox(height: 10.0),      
          Text(_titleCard(category), textAlign: TextAlign.center),
        ],
      ),
    );
  }

  String _titleCard(int n) {
    List titles = ['Música', 'Charlas', 'Medio ambiente', 'Cultura', 'Deporte', 'Otros'];
    return titles[n];
  }
}
