import 'package:cultural/src/pages/home_page.dart';
import 'package:cultural/widgets/toast.dart';
import 'package:flutter/material.dart';
// import 'package:login/src/components/toast.dart';
// import 'package:login/src/providers/login_provider.dart';

class LoginPage extends StatefulWidget {

  static final routeName = 'login';

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  // final LoginProvider loginProvider = LoginProvider();
  String email;
  String password;

  @override
  Widget build(BuildContext context) {

    final _screenSize = MediaQuery.of(context).size;

    return Scaffold(
      body: Stack(
        children: <Widget>[
          _background(_screenSize),
          _login(_screenSize),
        ],
      ),
    );
  }

  Widget _background(Size screenSize) {
    return Container(
      height: screenSize.height * 0.4,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only( bottomLeft: Radius.circular( 100.0 ) ),
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: <Color>[
            Color.fromRGBO(245 , 91, 0, 1.0),
            Color.fromRGBO(232 , 137, 22, 1.0),
          ],
        )
      ),
      child: Stack(
        children: <Widget>[
          Center(
            child: Image.asset('assets/hs-logo.png', height: screenSize.height * 0.2,),
          ),
          Container(
            padding: EdgeInsets.all(25.0),
            alignment: Alignment.bottomRight,
            height: double.maxFinite,
            width: double.maxFinite,
            child: Text('Login', style: TextStyle( color: Colors.white, fontSize: 25.0, fontWeight: FontWeight.bold )),
          ),
        ],
      ),
    );
    
  }

  Widget _login( Size screenSize ) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints viewportConstraints) {
        return SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: viewportConstraints.maxHeight,
            ),
            child: IntrinsicHeight(
              child: Column(
              children: <Widget>[
                Container(
                  height: screenSize.height * 0.4,
                ),
                SizedBox(height: 20.0),
                _createTextField('Email', false, Icons.email),
                _createTextField('Password', true, Icons.lock),
                Container(
                  alignment: Alignment.centerRight,
                  width: double.maxFinite,
                  child: Text('Forgot Password?'),
                ),
                SizedBox(height: 15.0),
                Expanded(
                  child: Container(),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 20.0),
                  child: Material(
                    elevation: 10.0, 
                    borderRadius: BorderRadius.circular(25.0),
                    color: Color.fromRGBO(232 , 137, 22, 1.0),
                    child: FlatButton(
                      padding: EdgeInsets.all( 15.0 ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0)
                      ),
                      color: Color.fromRGBO(232 , 137, 22, 1.0),
                      child: Container(
                        width: double.maxFinite,
                        alignment: Alignment.center,
                        child: Text('Login', style: TextStyle(color: Colors.white))
                      ),
                      onPressed: () {
                        _submit(context);
                        // Scaffold.of(context).showSnackBar(
                        //   SnackBar(
                        //     content: Text("ji"),
                        //   ),
                        // );
                      }
                    ),
                  ),
                ),
                Expanded(
                  child: Container(),
                ),
                Text('Don\'t have an account? Register'),
                SizedBox(height: 20.0),
              ],
            ),
          )
        )
        );
      }
    );
  }

  Widget _createTextField( String labelText, bool obscureText, IconData preIcon ) {
    return Container(
      margin: EdgeInsets.only(bottom: 20.0),
      child: Material(
        elevation: 7.0,
        borderRadius: BorderRadius.circular(30.0),
        shadowColor: Colors.black,
        child: TextField(
          obscureText: obscureText,
          decoration: InputDecoration(
            enabledBorder: InputBorder.none,
            prefixIcon: Icon( preIcon ),
            contentPadding: EdgeInsets.all(10.0),
            border: InputBorder.none,
            labelText: labelText,
          ),
          onChanged: (val) {
            if (labelText == 'Email') email = val;
            if (labelText == 'Password') password = val;
          },
        ),
      ),
    );
  }

  void _submit(BuildContext context) async {
    // final info = await loginProvider.login(email, password);
    
    // if ( info['ok'] ) {
      showToast(context, 'Bienvenido');

      Navigator.pushReplacementNamed(context, HomePage.routeName);
      
    // } else {
    //   showToast(context, info['message']);
    // }

  }
}