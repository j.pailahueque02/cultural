import 'dart:convert';

import 'package:cultural/src/models/event_model.dart';
import 'package:cultural/src/services/event_provider.dart';
import 'package:flutter/material.dart';

class EventPage extends StatefulWidget {
  @override
  _EventPageState createState() => _EventPageState();
}

class _EventPageState extends State<EventPage> {

  final EventProvider events = EventProvider();

  @override
  Widget build(BuildContext context) {
  final Size _screen = MediaQuery.of(context).size; 
    return Container(
      child: FutureBuilder(
        future: events.getEventsFirebase(),
        builder: (context, AsyncSnapshot<List<Event>> snapshot) {
          if (!snapshot.hasData) {
            return Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }

          final eventos = snapshot.data;
          return ListView.builder(
            itemCount: eventos.length,
            itemBuilder: (context, i) => _eventItem(eventos[i], _screen),
          );
        },
      ) 
      
      
    );
  }

  // Navigator.pushNamed(context, 'event_details', arguments: event),

  Widget _eventItem(Event event, Size screen) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 4.0),
      child: GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, 'event_details', arguments: event);
        },
        child: Card(
          child: Column(
            children: <Widget>[
              FadeInImage(
                image: NetworkImage(event.urlImagen),
                placeholder: AssetImage('assets/locationload.gif'),
                fit: BoxFit.fill,
                height: 220,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      width: double.maxFinite,
                      padding: EdgeInsets.only(left: 24.0, bottom: 5.0),
                      child: Text(event.titulo, textAlign: TextAlign.start),
                    ),
                    Row(
                      children: <Widget>[
                        Icon(Icons.location_on, color: Theme.of(context).primaryColor, size: 20.0),
                        SizedBox(width: 5.0),
                        Text(event.direccion),
                      ],
                    ),
                    SizedBox(height: 5.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Icon(Icons.calendar_today, color: Theme.of(context).primaryColor, size: 20.0),
                            SizedBox(width: 5.0),
                            Container(
                              width: screen.width * 0.65,
                              child: Text(event.fecha, overflow: TextOverflow.ellipsis)
                            )
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Icon(Icons.access_time, color: Theme.of(context).primaryColor, size: 20.0),
                            SizedBox(width: 5.0),
                            Container(
                              width: screen.width * 0.1,
                              child: Text(event.horario, overflow: TextOverflow.ellipsis)
                            )
                          ],
                        )
                      ],
                    ),
                  ]
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _getDataApi() async {
    JsonCodec codec = new JsonCodec();
    Map<String, Object> dat = await events.getEventsApi();
    List<dynamic> status = dat['status']; 
    Map<String, Object> data = status[0];

    String dataStr = data.toString();
    String newValue = dataStr.replaceAll('[u', '[')
                            .replaceAll('location', '"location"')
                            .replaceAll('latitude', '"latitude"')
                            .replaceAll('longitude', '"longitude"')
                            .replaceAll('""longitude""', '"longitude"')
                            .replaceAll('""latitude""', '"latitude"')
                            .replaceAll('extra', '"extra"')
                            .replaceAll(r'\', '')
                            .replaceAll('"Born to Die"', 'Born to Die')
                            .replaceAll('"Interpretacixf3n', 'Interpretacixf3n')
                            .replaceAll(' "La despedida', 'La despedida')
                            .replaceAll('Electrocardiograma"', 'Electrocardiograma')
                            ;
    Map<String, dynamic> decodeData = json.decode("""$newValue""");
    // Map<String, dynamic> decodeData = json.decode("""{"location": {"latitude": 121, "longitude": 1212}}""");
      // Map<String, Object> extra = json.decode( data['extra'] ); 
  }

  _getDataFb() async {

    List<Event> eventList = await events.getEventsFirebase();

    print(eventList);  
  }
}