import 'package:cultural/src/models/event_model.dart';
import 'package:cultural/widgets/map_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';

class EventDetailPage extends StatelessWidget {

  static final routeName = 'event_details';
  @override
  Widget build(BuildContext context) {
    final Size screen = MediaQuery.of(context).size; 
    final Event event = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: Text('Información'),
      ),
      body: ListView(
        children: <Widget>[
          _eventItem(context, event, screen),
          _location(event.latitud, event.longitud),
        ],
      ),
    );
  }

  Widget _eventItem(BuildContext context, Event event, Size screen) {
      return Column(
      children: <Widget>[
        FadeInImage(
          image: NetworkImage(event.urlImagen),
          placeholder: AssetImage('assets/locationload.gif'),
          fit: BoxFit.fill,
          height: 220,
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
          child: Column(
            children: <Widget>[
              Container(
                width: double.maxFinite,
                padding: EdgeInsets.symmetric(vertical: 10.0),
                child: Text(event.titulo, textAlign: TextAlign.center),
              ),
              SizedBox(height: 8.0),
              Row(
                children: <Widget>[
                  Icon(Icons.location_on, color: Theme.of(context).primaryColor, size: 20.0),
                  SizedBox(width: 5.0),
                  Text(event.direccion),
                ],
              ),
              SizedBox(height: 10.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Icon(Icons.calendar_today, color: Theme.of(context).primaryColor, size: 20.0),
                      SizedBox(width: 5.0),
                      Container(
                          width: screen.width * 0.65,
                          child: Text(event.fecha, overflow: TextOverflow.ellipsis)
                        )
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Icon(Icons.access_time, color: Theme.of(context).primaryColor, size: 20.0),
                      SizedBox(width: 5.0),
                      Container(
                        width: screen.width * 0.1,
                        child: Text(event.horario, overflow: TextOverflow.ellipsis)
                      )
                    ],
                  )
                ],
              ),
              SizedBox(height: 28.0),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                child: Text(event.descripcion,
                  textAlign: TextAlign.justify,                
                ),
              )
            ]
          ),
        ),
      ],
    );
  }

  Widget _location(double latitud, double longitud) {
    return Container(
      height: 350.0,
      margin: EdgeInsets.symmetric(vertical: 15.0),
      child: FlutterMap(
        options: new MapOptions(
          center: new LatLng(latitud, longitud),
          zoom: 13.0,
        ),
        layers: [
          map(),
          _createMarkers(new LatLng(latitud, longitud)),
        ],
      ),
    );
  }

  
  _createMarkers(LatLng ltlg) {
    return MarkerLayerOptions(
      markers: <Marker>[
        Marker(
          height: 200.0,
          width: 200.0,
          point: ltlg,
          builder: (context) => Center(
            child: Icon(Icons.location_on, color: Colors.red, size: 40.0),
          )
        ),
      ]
    );
  }
}