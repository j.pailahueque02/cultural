import 'package:cultural/src/models/event_model.dart';
import 'package:cultural/src/services/event_provider.dart';
import 'package:cultural/widgets/map_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';

class MapEventPage extends StatefulWidget {
  @override
  _MapEventPageState createState() => _MapEventPageState();
}

class _MapEventPageState extends State<MapEventPage> {
  final EventProvider events = EventProvider();

  @override
  Widget build(BuildContext context) {

    return Container(
      child: FutureBuilder( 
        future: events.getEventsFirebase(),
        builder: (context, AsyncSnapshot<List<Event>> snapshot) {

          if (!snapshot.hasData) return Container(child: Center(child: CircularProgressIndicator())); 

          return FlutterMap(
            options: new MapOptions(
              center: new LatLng(-38.746202450627074, -72.61593475779421),
              zoom: 13.0,
            ),
            layers: _createLayers(snapshot.data),
          );
        }

      ),
    );
  }

  _createMarkers(LatLng ltlg, Event event) {
    return MarkerLayerOptions(
      markers: <Marker>[
        Marker(
          height: 200.0,
          width: 200.0,
          point: ltlg,
          builder: (context) => Center(
            child: Container(
              child: Stack( 
                children: <Widget>[
                  Center(
                    child: Icon(Icons.location_on, color: Colors.red, size: 40.0),
                  ),
                  Positioned(
                    child: Card(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Container(
                                  width: 150,
                                  child: Text(event.titulo, textAlign: TextAlign.center, overflow: TextOverflow.clip),
                                ),
                              ],
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(Icons.calendar_today, color: Theme.of(context).primaryColor, size: 18.0),
                              Container(
                                width: 150,
                                child: Text(event.fecha, textAlign: TextAlign.center, overflow: TextOverflow.ellipsis),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(Icons.access_time, color: Theme.of(context).primaryColor, size: 20.0),
                              Container(
                                width: 150,
                                child: Text(event.horario, textAlign: TextAlign.center, overflow: TextOverflow.clip),
                              ),
                            ],
                          )
                        ],
                      ),
                    )
                  ),
                ],
              ),
            ),
          )
        ),
      ]
    );
  }

  _createLayers(List<Event> events) {
    List<LayerOptions> layers = [];
    layers.add(map());

    events.forEach((ev) {
      layers.add(_createMarkers(new LatLng(ev.latitud, ev.longitud), ev));
    });

    return layers;
  }
}