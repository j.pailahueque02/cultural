import 'package:flutter/material.dart';

class EventCategoryPage extends StatefulWidget {
  @override
  _EventCategoryState createState() => _EventCategoryState();

  static final routeName = 'event_category';
}

class _EventCategoryState extends State<EventCategoryPage> {
  @override
  Widget build(BuildContext context) {
    
    final category = ModalRoute.of(context).settings.arguments;
    
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(category),
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            _eventItem(),
            _eventItem(),
            _eventItem(),
            _eventItem(),
            _eventItem(),
            _eventItem(),
            _eventItem(),
            _eventItem(),
          ],
        ),
      ),
    );
  }

  // Navigator.pushNamed(context, 'event_details', arguments: event),

  Widget _eventItem() {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, 'event_details', arguments: 'hi');
      },
      child: Card(
        child: Column(
          children: <Widget>[
            FadeInImage(
              image: NetworkImage('https://as.com/meristation/imagenes/2019/12/11/noticias/1576082964_310148_1576083189_noticia_normal.jpg' ),
              placeholder: AssetImage('assets/locationload.gif'),
              fit: BoxFit.fill,
              height: 220,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
              child: Column(
                children: <Widget>[
                  Container(
                    width: double.maxFinite,
                    padding: EdgeInsets.only(left: 24.0, bottom: 5.0),
                    child: Text('The Game Awards', textAlign: TextAlign.start),
                  ),
                  Row(
                    children: <Widget>[
                      Icon(Icons.location_on, color: Theme.of(context).primaryColor, size: 20.0),
                      SizedBox(width: 5.0),
                      Text('Francisco Salazar 8127'),
                    ],
                  ),
                  SizedBox(height: 5.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(Icons.calendar_today, color: Theme.of(context).primaryColor, size: 20.0),
                          SizedBox(width: 5.0),
                          Text('sábado 14 de marzo 2020')
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Icon(Icons.access_time, color: Theme.of(context).primaryColor, size: 20.0),
                          SizedBox(width: 5.0),
                          Text('20:00')
                        ],
                      )
                    ],
                  ),
                ]
              ),
            )
          ],
        ),
      ),
    );
  }
}