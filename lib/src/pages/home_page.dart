import 'package:cultural/src/pages/category_page.dart';
import 'package:cultural/src/pages/recycle.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/fa_icon.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'event/event_map_page.dart';
import 'event/event_page.dart';

class HomePage extends StatefulWidget {
  static final routeName = "home";

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentPage = 0;

  @override
  Widget build(BuildContext context) {
    // Size _screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('App cultural'),
      ),
      body: _callPage( _currentPage ),
      bottomNavigationBar: _bottomNavBar(),
    );
  }


  
  Widget _bottomNavBar() {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      currentIndex: _currentPage,
      onTap: (index) {
        setState(() {
          _currentPage = index;
        });
      },
      items: [
        BottomNavigationBarItem(
          icon: FaIcon(FontAwesomeIcons.calendarWeek),
          title: Text('Eventos'),
        ),
        BottomNavigationBarItem(
          icon: FaIcon(FontAwesomeIcons.listUl),
          title: Text('Categorias'),
        ),
        BottomNavigationBarItem(
          icon: FaIcon(FontAwesomeIcons.mapMarkedAlt),
          title: Text('Mapa'),
        ),
        BottomNavigationBarItem(
          icon: FaIcon(FontAwesomeIcons.recycle),
          title: Text('Reciclaje'),
        ),
      ],
    );
  }

  Widget _callPage( int currentPage ) {
    switch(currentPage) {
      case 0: return EventPage();
      case 1: return CategoryPage();
      case 2: return MapEventPage();
      case 3: return RecyclePage();

      default: return EventPage();
    }
  }
}
